# base image
FROM python:3.8-slim

# add a new user to run the container as a non-root user
# and set bash as the default shell 
RUN useradd --create-home --shell /bin/bash app_user

# set the work directory as the users home dir
WORKDIR /home/app_user

# change to the new user
USER app_user

# copy source
COPY ./app/src .

# command to run on container start
CMD [ "bash" ]
