'''
Simple Class to hold blog posts by title and author.
'''
# Program: blog.py
# Author: Jason Hawthorne
# Created Date: 2021-05-25

from post import Post

class Blog:
    '''
    Main Class for this blog app.
    '''
    def __init__(self, title, author):
        '''
        Class initializer
        :param title: title of the blog
        :param author: author of the blog
        '''
        self.title = title
        self.author = author
        self.posts = []

    def __repr__(self) -> str:
        '''
        Method that returns the class object as a string
        '''
        return '{} by {} ({} post{})'.format(self.title,
                                             self.author,
                                             len(self.posts),
                                             's' if len(self.posts) !=1 else '')

    def create_post(self, title, content):
        '''
        Method to create a post object
        '''
        self.posts.append(Post(title, content))

    def json(self):
        '''
        Returns the blog as a JSON object
        '''
        return {
            'title': self.title,
            'author': self.author,
            'posts': [post.json() for post in self.posts]
        }
