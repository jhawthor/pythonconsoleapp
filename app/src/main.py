'''
Entry point for the blog app containing the menu.
'''
# Program: blog.py
# Author: Jason Hawthorne
# Created Date: 2021-05-27

from blog import Blog


MENU_PROMPT = '''
Select an option:
"c" to create a blog
"l" to list them
"r" to read a blog
"p" to write a post
"q" to quit: '''
POST_TEMPLATE = '''
--- {} ---

{}

'''

# Dictionary to hold the blogs
# [(blog_name, Blog), (blog_name, Blog), etc.]
blogs = dict()

def menu():
    '''
    Main menu that controls all options for this Simple
    console blog application
    '''
    # Show available blogs
    # Allow the user to make a choice
    # Do something with the choice
    # Exit the application when 'q' is entered

    print_blogs()
    selection = input(MENU_PROMPT)
    while selection != 'q':
        if selection == 'c':
            ask_create_blog()
        elif selection == 'l':
            print_blogs()
        elif selection == 'r':
            ask_read_blog()
        elif selection == 'p':
            ask_create_post()
        selection = input(MENU_PROMPT)

def print_blogs():
    '''
    Function that will print all blogs in a bullet format
    '''
    for key, blog in blogs.items():
        print('- {}'.format(blog))

def ask_create_blog():
    '''
    Create a blog by asking for thr title and author
    '''
    title = input('Enter a title for the blog: ')
    author = input('Enter you name: ')
    blogs[title] = Blog(title, author)

def ask_read_blog():
    '''
    Display the blog based on the title
    '''
    title = input('Enter the blog title: ')
    print_posts(blogs[title])

def print_posts(blog):
    '''
    Display all the posts in a blog
    '''
    for post in blog.posts:
        print_post(post)

def print_post(post):
    '''
    Display the post using a template
    param: post: a specific blog post
    '''
    print(POST_TEMPLATE.format(post.title, post.content))

def ask_create_post():
    '''
    Create a post
    '''
    blog_name = input('Enter the blog title you want to write a post in: ')
    title = input('Enter your post title: ')
    content = input('Enter your post content: ')
    blogs[blog_name].create_post(title, content)

if __name__ == "__main__":
    # execute only if run as a script
    menu()
