'''
Simple Class to hold posts for a blog.
'''
# Program: blog.py
# Author: Jason Hawthorne
# Created Date: 2021-05-25

class Post:
    '''
    Main Class for posts.
    '''
    def __init__(self, title, content):
        '''
        Class initializer
        :param title: title of the post
        :param content: content of the post
        '''
        self.title = title
        self.content = content

    def json(self):
        '''
        Returns the post as a JSON object
        '''
        return {
            'title': self.title,
            'content': self.content,
        }
