'''
    Integration/Interoperability Tests
'''

from unittest import TestCase
import sys
import os
sys.path.insert(1, os.path.join(os.path.abspath(
    os.path.join(os.path.dirname(__file__), '../..')), 'src'))
from blog import Blog

BLOG_TITLE = 'Test'
BLOG_AUTHOR = 'Test Author'
POST_TITLE = 'Test Post'
POST_CONTENT = 'Test Content'

class BlogTest(TestCase):
    '''
    Integration tests for this Simple Blog Application
    which is testing the integration of the Post and Blog classes.
    In general, this would not be considered as interoperability testing
    because we are using a middle-man process to convert the dictionary object
    to a JSON object.
    '''

    #//tt: Statememt Testing, sqc: Interoperability, req: 0, ac: AC0
    def test_create_post_in_blog(self):
        '''
        Testing the ability to create a post in a blog
        '''
        blog = Blog(BLOG_TITLE, BLOG_AUTHOR)
        blog.create_post(POST_TITLE, POST_CONTENT)
        self.assertEqual(len(blog.posts), 1)
        self.assertEqual(blog.posts[0].title, POST_TITLE)
        self.assertEqual(blog.posts[0].content, POST_CONTENT)

    #//tt: Statememt Testing, sqc: Interoperability, req: 0, ac: AC0
    def test_blog_repr(self):
        '''
        Testing the __repr__ method used to compute a printable representation
        of a blog object.
        '''
        blog = Blog(BLOG_TITLE, BLOG_AUTHOR)
        self.assertEqual(str(blog), 'Test by Test Author (0 posts)')

    #//tt: Statememt Testing, sqc: Interoperability, req: 0, ac: AC0
    def test_json_with_no_posts(self):
        '''
        Testing the ability to create a JSON blog object with no post objects
        '''
        blog = Blog(BLOG_TITLE, BLOG_AUTHOR)
        expected = {'title': BLOG_TITLE, 'author': BLOG_AUTHOR, 'posts': []}
        self.assertDictEqual(expected, blog.json())

    #//tt: Statememt Testing, sqc: Integration, req: 0, ac: AC0
    def test_json_with_posts(self):
        '''
        Testing the ability to create a JSON blog object with post objects in JSON format
        '''
        blog = Blog(BLOG_TITLE, BLOG_AUTHOR)
        blog.create_post(POST_TITLE, POST_CONTENT)
        self.assertDictEqual(blog.json(), {
            'title': blog.title,
            'author': blog.author,
            'posts': [{'title': POST_TITLE, 'content': POST_CONTENT}],
        })
