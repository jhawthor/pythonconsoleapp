'''
    System Tests
'''

from unittest import TestCase
from unittest.mock import patch
import sys
import os
sys.path.insert(1, os.path.join(os.path.abspath(
    os.path.join(os.path.dirname(__file__), '../..')), 'src'))
from blog import Blog
import main
from post import Post

BLOG_TITLE = 'Test'
BLOG_AUTHOR = 'Test Author'
POST_TITLE = 'Test Post'
POST_CONTENT = 'Test Content'
INPUT = 'builtins.input'
PRINT = 'builtins.print'

class MainTest(TestCase):
    '''
    System tests for this Simple Blog mainlication.
    Testing how all components work together that
    make up the mainlications functionality.
    '''

    # //tt: Statement Testing, sqc: Functional Correctness, req: 0, ac: AC0
    def test_menu_calls_create_blog(self):
        '''
        Verifying that the menu operation will call create blog
        when the correct input is provided
        '''
        with patch(INPUT) as mocked_input:
            with patch('main.ask_create_blog') as mocked_ask_create_blog:
                mocked_input.side_effect = ('c', 'q')
                main.menu()
                mocked_ask_create_blog.assert_called()

    # //tt: Statement Testing, sqc: Functional Correctness, req: 0, ac: AC0
    def test_menu_calls_print_blogs(self):
        '''
        Verifying that the menu operation will call print blog
        when the correct input is provided
        '''
        with patch(INPUT) as mocked_input:
            with patch('main.print_blogs') as mocked_ask_print_blogs:
                mocked_input.side_effect = ('l', 'q')
                main.menu()
                mocked_ask_print_blogs.assert_called()

    # //tt: Statement Testing, sqc: Functional Correctness, req: 0, ac: AC0
    def test_menu_calls_ask_read_blogs(self):
        '''
        Verifying that the menu operation will call read blogs
        when the correct input is provided
        '''
        with patch(INPUT) as mocked_input:
            with patch('main.ask_read_blog') as mocked_ask_read_blogs:
                mocked_input.side_effect = ('r', 'q')
                main.menu()
                mocked_ask_read_blogs.ask_read_blog()

    # //tt: Statement Testing, sqc: Functional Correctness, req: 0, ac: AC0
    def test_menu_calls_ask_create_post(self):
        '''
        Verifying that the menu operation will call create post
        when the correct input is provided
        '''
        with patch(INPUT) as mocked_input:
            with patch('main.ask_create_post') as mocked_aks_create_post:
                mocked_input.side_effect = ('p', 'q')
                main.menu()
                mocked_aks_create_post.ask_create_post()

    # //tt: Statement Testing, sqc: Functional Correctness, req: 0, ac: AC0
    def test_menu_prints_prompt(self):
        '''
        Verifying that the menu operation show the correct prompt.
        Using patch to mock the input method.
        '''
        with patch(INPUT, return_value='q') as mocked_input:
            main.menu()
            mocked_input.assert_called_with(main.MENU_PROMPT)

    #//tt: Statement Testing, sqc: Functional Correctness, req: 0, ac: AC1
    def test_print_blogs(self):
        '''
        Verifying that the print_blogs method can print the blogs
        Note: The format of the printed blog will be the format of the "__repr__"
        method in blog.py. Hence we are checking the the print method was called
        with the correct value.
        Using patch to mock the print function
        '''
        blog = Blog(BLOG_TITLE, BLOG_AUTHOR)
        main.blogs = {BLOG_TITLE: blog}
        with patch(PRINT) as mocked_print:
            main.print_blogs()
            mocked_print.assert_called_with('- Test by Test Author (0 posts)')

    #//tt: Statement Testing, sqc: Functional Completeness, req: 0, ac: AC0
    def test_ask_create_blog(self):
        '''
        Verify that the menu operation can create a blog.
        Using patch to mock the input method.
        Using patch.side_effect to "load" the mocked_input return values
        '''
        with patch(INPUT) as mocked_input:
            mocked_input.side_effect = (BLOG_TITLE, BLOG_AUTHOR)
            main.ask_create_blog()
            self.assertIsNotNone(main.blogs.get(BLOG_TITLE))

    #//tt: Statement Testing, sqc: Functional Completeness, req: 0, ac: AC0
    def test_ask_read_blog(self):
        '''
        Verify that ask_read_blog calls print posts.
        '''
        blog = Blog(BLOG_TITLE, BLOG_AUTHOR)
        main.blogs = {BLOG_TITLE: blog}
        with patch(INPUT, return_value=BLOG_TITLE):
            with patch('main.print_posts') as mocked_print_posts:
                main.ask_read_blog()
                mocked_print_posts.assert_called_with(blog)

    #//tt: Statement Testing, sqc: Functional Completeness, req: 0, ac: AC0
    def test_print_posts(self):
        '''
        Verify that print_posts is called with the correct post object
        '''
        blog = Blog(BLOG_TITLE, BLOG_AUTHOR)
        blog.create_post(POST_TITLE, POST_CONTENT)
        main.blogs = {BLOG_TITLE: blog}
        with patch('main.print_post') as mocked_print_post:
            main.print_posts(blog)
            mocked_print_post.assert_called_with(blog.posts[0])

    #//tt: Statement Testing, sqc: Functional Correctness, req: 0, ac: AC0
    def test_print_post(self):
        '''
        Verify that print_post prints the post in the template format
        '''
        post = Post('Post Title', 'Post Content')
        expected_print = '''
--- Post Title ---

Post Content

'''
        with patch(PRINT) as mocked_print:
            main.print_post(post)
            mocked_print.assert_called_with(expected_print)

    #//tt: Statement Testing, sqc: Functional Completeness, req: 0, ac: AC0
    def test_create_post(self):
        '''
        Verify that the a post can be created
        '''
        blog = Blog(BLOG_TITLE, BLOG_AUTHOR)
        main.blogs = {BLOG_TITLE: blog}
        with patch(INPUT) as mocked_input:
            mocked_input.side_effect = (BLOG_TITLE, 'Test Title', POST_CONTENT)
            main.ask_create_post()
            self.assertEqual(blog.posts[0].title, 'Test Title')
            self.assertEqual(blog.posts[0].content, POST_CONTENT)
