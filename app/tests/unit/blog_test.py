'''
Unit tests for the Post Class
'''
from unittest import TestCase
import sys
import os
sys.path.insert(1, os.path.join(os.path.abspath(
    os.path.join(os.path.dirname(__file__), '../..')), 'src'))
from blog import Blog

BLOG_TITLE = 'Test'
BLOG_AUTHOR = 'Test Author'

class BlogTest(TestCase):
    '''
    Unit tests for the Blog Class of this Simple Blog Application
    '''

    #//tt: Statement Testing, sqc: Functional Correctness, req: 0, ac: AC0
    def test_create_blog(self):
        '''
        Testing the ability to create a blog
        with 0 posts
        '''
        blog = Blog(BLOG_TITLE, BLOG_AUTHOR)
        self.assertEqual(BLOG_TITLE, blog.title)
        self.assertEqual(BLOG_AUTHOR, blog.author)
        self.assertListEqual([], blog.posts)
        self.assertEqual(0, len(blog.posts))

    #//tt: Statement Testing, sqc: Functional Completeness, req: 0, ac: AC0
    def test_repr(self):
        '''
        Testing the __repr__ method used to compute a printable representation
        of a blog object when debugging.
        '''
        blog = Blog(BLOG_TITLE, BLOG_AUTHOR)
        blog2 = Blog('My Day','Rollins')
        self.assertEqual(blog.__repr__(), 'Test by Test Author (0 posts)')
        self.assertEqual(blog2.__repr__(), 'My Day by Rollins (0 posts)')

    #//tt: Statement Testing, sqc: Functional Completeness, req: 0, ac: AC0
    def test_repr_multiple_posts(self):
        '''
        Testing the ability to create a blog
        with 1 or more posts
        '''
        blog = Blog(BLOG_TITLE, BLOG_AUTHOR)
        blog.posts = ['Test2']
        blog2 = Blog('My Day','Rollins')
        blog2.posts = ['Test2', 'another']
        self.assertEqual(blog.__repr__(), 'Test by Test Author (1 post)')
        self.assertEqual(blog2.__repr__(), 'My Day by Rollins (2 posts)')
