'''
Unit tests for the Post Class
'''
from unittest import TestCase
import sys
import os
sys.path.insert(1, os.path.join(os.path.abspath(
    os.path.join(os.path.dirname(__file__), '../..')), 'src'))
from post import Post

POST_TITLE = 'Test Post'
POST_CONTENT = 'Test Content'

class PostTest(TestCase):
    '''
    Unit tests for the Post Class of this Simple Blog Application
    '''

    #//tt: Statement Testing, sqc: Functional Correctness, req: 0, ac: AC0
    def test_create_post(self):
        '''
        Testing the ability to create a Post
        '''
        my_post = Post(POST_TITLE, POST_CONTENT)
        self.assertEqual(POST_TITLE, my_post.title)
        self.assertEqual(POST_CONTENT, my_post.content)

    #//tt: Statement Testing, sqc: Functional Completeness, req: 0, ac: AC0
    def test_json(self):
        '''
        Testing the ability to create a JSON object
        from the dictionary Post object.
        '''
        my_post = Post(POST_TITLE, POST_CONTENT)
        expected = {'title': POST_TITLE, 'content': POST_CONTENT}
        self.assertDictEqual(expected, my_post.json())
